import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  base: '/cb_publish_gitlab_pages_vite_react/',
  plugins: [react()],
})
